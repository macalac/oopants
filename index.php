<?php

require_once 'autoload.php';

$jar = \lib\SessionHandler::getJar();

if (isset($_POST['poke'])) {
    $jar->returnItem($_POST['poke'])->poke();
} elseif (isset($_POST['heal'])) {
    $jar->returnItem($_POST['heal'])->heal();
}

$view = new OOPants\View();

$view->setFormAction('index.php')
    ->renderCollection($jar);
