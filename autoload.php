<?php

/**
 * Simple auto loader.
 * @param $ClassName
 */
function autoload($ClassName) {
    $Path = str_replace('\\', '/', $ClassName);
    if (is_readable('Classes/'.$Path.'.php')) {
        require_once 'Classes/'.$Path.'.php';
    } else {
        die('Class '.$ClassName.' not found.');
    }
}

spl_autoload_register('autoload');
