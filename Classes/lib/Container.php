<?php
/**
 * Created by PhpStorm.
 * User: madamarczuk
 * Date: 09/12/2013
 * Time: 14:18
 */

namespace lib;

class container
{
    protected $_items = array();
    protected $_size = '6';

    /**
     * @return string
     */
    public function getSize()
    {
        return $this->_size;
    }

    public function setSize($size)
    {
        $this->_size = $size;
        return $this->_size;
    }

    public function __construct($numOfAnts = 6)
    {
        $this->setSize($numOfAnts);
        $this->catchAnts();
    }

    protected function catchAnts() {
        for ($i = 1; $i <= $this->getSize(); $i++) {
            $ant = new \OOPants\ant();
            $this->_items[$ant->getName()] = $ant;
        }
    }

    /**
     * @return array
     */
    public function getAll() {
        return $this->_items;
    }

    /**
     * @param $name \OOPants\ant
     * @return mixed
     */
    public function returnItem($name)
    {
        return $this->_items[$name];
    }
}
