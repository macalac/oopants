<?php
/**
 * Created by PhpStorm.
 * User: madamarczuk
 * Date: 10/12/2013
 * Time: 11:16
 */

namespace lib;


class SessionHandler {
    /**
     * @return \lib\container
     */
    static function getJar()
    {
        self::_initSession();

        if (!(isset($_SESSION['jar']) && $_SESSION['jar'] instanceof \lib\container))
        {
            $_SESSION['jar'] = new \lib\container();
        }
        return $_SESSION['jar'];
    }

    /**
     *
     */
    static public function _initSession()
    {
        if (!isset($_SESSION)) { session_start(); }
    }
}
