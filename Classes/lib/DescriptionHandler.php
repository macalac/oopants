<?php
/**
 * Created by PhpStorm.
 * User: madamarczuk
 * Date: 12/12/2013
 * Time: 11:09
 */

namespace lib;


class DescriptionHandler {

    static protected $_output = 'Default Output';

    /**
     * @param string $param
     * @return bool
     */
    static function getOutput($param = 'death') {
        switch ($param) {
            case 'death':
                return self::$_deathText[array_rand(self::$_deathText)];
                break;
            case 'heal':
                return self::$_healText[array_rand(self::$_healText)];
                break;
            case 'Other':
                default:
                return false;
                break;
        }
    }

    static protected $_deathText = array('is dead!',
                                         'took an arrow to the knee!',
                                         'popped!',
                                         'got knocked out!',
                                         'kicked the bucket!'
                                        );
} 