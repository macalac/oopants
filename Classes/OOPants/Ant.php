<?php

namespace OOPants;
use lib;
class ant {
    /**
     * @var int
     */
    protected $_health = 100;

    /**
     * @var string
     */
    protected $_name = 'Default';
    /**
     * @var string
     */
    protected $_lastWords = '';

    /**
     * @return string
     */
    public function getLastWords()
    {
        if (!$this->_lastWords) {
            $deathOutput = lib\DescriptionHandler::getOutput('death');
            $this->_setLastWords($deathOutput);
        }
        return $this->_lastWords;
    }

    /**
     * @param string $lastWords
     */
    protected function _setLastWords($lastWords)
    {
        $this->_lastWords = $lastWords;
    }

    /**
     * Magic function gives ant a name
     */
    public function __construct(){
        $string = (string)rand(0, 1000);
        $this->setName($string);
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->_name = $name;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->_name;
    }

    /**
     * @return int
     */
    public function getHealth(){
        return $this->_health;
    }

    /**
     * @param null $int
     */
    protected function _increaseHealth($int = null){
        $this->_health += ($int === null) ? 1 : abs((int)$int);
    }

    /**
     * @param null $int
     */
    protected function _decreaseHealth($int = null){
        $this->_health -= ($int === null) ? 1 : abs((int)$int);
    }

    /**
     * @return bool
     */
    public function isAlive(){
        return !($this->getHealth() <= 0);
    }

    /**
     * @return string
     */
    public function __toString(){
        return ucfirst($this->_name);
    }

    /**
     *
     */
    public function poke() {
        $int = rand(0,100);
        $this->_decreaseHealth($int);
    }

    /**
     *
     */
    public function heal() {
        $int = rand(0,100);
        $this->_increaseHealth($int);
    }

    /**
     * @return string
     */
    public function getForms() {
        $output = '<form method="post" action="index.php">';
        if ($this->isAlive()) {
            $output .= $this->_getPokeCode();
            $output .= $this->_getHealCode();
        } else {
            $output .= $this->_getHealCode();
        }
        $output .= '</form>';
        return $output;
    }

    protected function _getPokeCode() {
        $output = '<button name="poke" value="' . $this->getName() . '"type="submit">Poke</button>';
        return $output;
    }

    protected function _getHealCode() {
        $output = '<button name="heal" value="' . $this->getName() . '" type="submit">Heal</button>';
        return $output;
    }
}
