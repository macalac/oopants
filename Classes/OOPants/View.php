<?php

namespace OOPants;


use lib\container;

class View {

    protected $_formAction = 'index.php';

    /**
     * @param \lib\container $jar
     * @param bool $echo
     * @return void|string
     */
    public function renderCollection(container $jar, $echo = TRUE) {
        $output = '<ul>';
        foreach ($jar->getAll() as $ant) {
            /**
             * @var ant $ant
             */
            $output .= '<li>';
            $output .= 'Ant ' . $ant->getName() . ' ';
            if ($ant->isAlive()) {
                $output .= 'Health: ' . $ant->getHealth();
            } else {
                $output .= $ant->getLastWords();
            }
            $output .= $ant->getForms();
            $output .= '</li>';
        }
        $output .= '</ul>';

        if ($echo === TRUE) {
            echo $output;
        }
        else {
            return $output;
        }
    }

    /**
     * @return string
     */
    public function getFormAction() {
        return $this->_formAction;
    }

    /**
     * @param string $formAction
     * @return $this
     */
    public function setFormAction($formAction) {
        $this->_formAction = $formAction;
        return $this;
    }
}
